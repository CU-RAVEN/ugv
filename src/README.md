# RAVEN Project - UGV SRC Files
## Rover and Air Environment Navigation

This folder contains all of the developed code to run on the Jackal UGV in order 
to allow for successful integration and safe operation with the RAVEN system

These nodes are are:
- __ugv_watchdog.cpp__ The watchdog node ensures the safe operation of the UGV by checking that several conditions are being met. Specifically, the watchdog node does the following:
    - Checks that the Jackal Battery voltage is above 24.5 V
    - Checks that the Motor temperatures are less than 80C
    - Checks that the MCU/PCB temperatures are less than 70C
    - Checks that the Jackal is still connected to the WiFi network
    - Checks that a Emergency Flag was not set manually
    - Determines and publishes network quality statistics
    - Stops the Jackal's movement if the above are violated by overwriting the cmd_vel topic
- __ugv_housekeeping.cpp__ node used for output topics to the UI
    - Reduces the publishing rate of certain topics to roughly 5Hz
    - Determines and publishes storage information of mounted drives
