/*
*
*
*    Node: UGV Watchdog 
*    Date: 02/08/2018
*    Author: Rishab Gangopadhyay
*
*    Vehicle: UGV
*
*
*    Purpose: The UGV Watchdog will ensure that certain telemetry threshold
*             and aspects of the UGV operation are not violated in order
*             to ensure that the UGV response is as expected
*
*
*    Conditions that would cause an emergency safing:
*          1. Jackal UGV Battery Voltage < 24.5 V
*          2. PCB/MCU Temperature > 70.0 C
*          3. Motor Temperatures  > 80.0 C
*          4. WiFi Disconnected
*          5. Emergency Safe Manually Set (Circle button on Dualshock4 Controller)
*
*    Actions:
*         If the emergency safing flag is set, a boolean topic (/ugv_emergency)
*         will be set to TRUE. Additionally, the watchdog node will publish out
*         zeros to the commanded velocity to stop the UGV. The timeout flag can 
*         be reset by restarting the Jackal OR pressing Triangle on the Dualshock 4
*         controller once the other thresholds have been met.
*
*/
/* ********************************************************************************
 *  Packages to Include 
 *
 **********************************************************************************/ 

#include <ros/ros.h>
#include <math.h>
#include <tf/transform_listener.h>
#include "sensor_msgs/Joy.h"
#include "geometry_msgs/Twist.h"
#include "jackal_msgs/Status.h"
#include "jackal_msgs/Feedback.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"


/* ********************************************************************************
 *  Variable Definitions
 *
 **********************************************************************************/ 

// Timeout Emergency Flag; Is set when thresholds met
bool timeoutBool = 0;                                
// Initialize msgs to be able to read from Diagnostics Topic

//diagnostic_msgs::DiagnosticStatus cmdratestatus ;
//diagnostic_msgs::KeyValue cmdratekeyvalue;
//std::string cmdrateHz;

//Diagnostics Variables Initialization
float battV;
float pcb_temp;
float mcu_temp;
float motor0_temp;
float motor1_temp;


//Variables used to get network status
std::string network_status;
vector<string> network_data;
bool wifiConnected;
geometry_msgs::Vector3 network_msg;
const char networkcmdarr[] = "head -3 /proc/net/wireless | tail -1  | awk '{print $3 $4 $5}' 2>&1";

//Timeout Message
std_msgs::Bool timeoutmsg;

// SET THRESHOLDS
float lowVoltageThresh = 24.5; //V
float pcb_tempThresh   = 70.0; //C
float mcu_tempThresh   = 70.0; //C
float motor_tempThresh = 80.0; //C

/* ********************************************************************************
 *  Callback Functions 
 *
 *  These callback functions are called by the ROS subscribers defined in main 
 *  whenever a new topic value comes in. The primary role of these callback 
 *  functions is to store a local copy of the most recent telemetry so that they
 *  can be output as required to maintain a 5Hz output
 **********************************************************************************/ 

// TempSubCallback: Used to get temperature from motor feedback topic
void TempSubCallback(const jackal_msgs::Feedback::ConstPtr& msg)
{
    
    pcb_temp = msg->pcb_temperature;
    mcu_temp = msg->mcu_temperature;
    motor0_temp = msg->drivers[0].motor_temperature;   
    motor1_temp = msg->drivers[1].motor_temperature;

}

// BatterySubCallback: Used to save off battery status 
void BatterySubCallback(const jackal_msgs::Status::ConstPtr& msg)
{
    battV = msg->measured_battery;
}


// Callback function to get Jackal Wifi Connection status
void WiFiSubCallback(const std_msgs::Bool::ConstPtr& msg)
{
    wifiConnected = msg->data;
}

//Callback function for inputs from the Dualshock 4 Controller
void BluetoothSubCallback(const sensor_msgs::Joy::ConstPtr& msg)
{   

    
    //If circle, set timeout flag
    if (msg->buttons[1] == 1)
    {
        timeoutBool = 1;
    }
    //If triangle, clear timeout flag 
    if (msg->buttons[3] == 1)
    {
        timeoutBool = 0;
    }

}




/* ********************************************************************************
 *  Helper Functions 
 *
 **********************************************************************************/ 

/* getCmdOutput
*
*  Purpose: to run a command line argument and store the output locally 
*  Modified from code by Vittorio Romeo 
*  https://codereview.stackexchange.com/questions/42148/
*  running-a-shell-command-and-getting-output */
std::string getCmdOutput(const std::string& mStr)
{
    std::string result, file;
    FILE* pipe{popen(mStr.c_str(), "r")};
    char buffer[256];

    while(fgets(buffer, sizeof(buffer), pipe) != NULL)
    {
        file = buffer;
        result += file.substr(0, file.size() - 1);
    }

    pclose(pipe);
    return result;
}

/* split
*
* Purpose: to take a string variable and split it into its various elements
*          by some predetermined delimiter
 http://ysonggit.github.io/coding/2014/12/16/split-a-string-using-c.html */
vector<string> split(const string &s, char delim) {
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}


/* check_battV
*
*   Check if the battery voltage is within thresholds
*   If not, set the timeout boolean flag
*/
void check_battV()
{
    if (battV <= lowVoltageThresh && battV >= 1)
    {
        ROS_INFO("WARNING: LOW UGV VOLTAGE");
        timeoutBool = 1;
    }
}

/* check_temperatures
*
*   Check if any temperature limits are being broken
*   If so, set the timeout boolean flag
*/
void check_temperature()
{
    
    if (pcb_temp >= pcb_tempThresh)
    {
        ROS_INFO("WARNING: PCB TEMPERATURE HIGH");
        timeoutBool = 1;
    }

    if (mcu_temp >= mcu_tempThresh)
    {
        ROS_INFO("WARNING: MCU TEMPERATURE HIGH");
        timeoutBool = 1;
    }

    if (motor0_temp >= motor_tempThresh || motor1_temp >= motor_tempThresh)
    {
        ROS_INFO("WARNING: MOTOR TEMPERATURE HIGH");
        timeoutBool = 1;
    }
}

/* checkWiFi
*
*   Check if the Jackal is still connected to the network
*   If not, set the timeout boolean flag
*/
void checkWiFi()
{
    if (!wifiConnected)
    {
        ROS_INFO("WARNING: NO WIFI CONNECTION DETECTED");
        timeoutBool = 1;
    }
    else 
    {
        timeoutBool = 0;
    }
    //checkWiFiStrength();
}

/* checkWiFiStrength
*
*  Purpose: this function runs a command line argument to read in information
*           from the Linux filesystems's /proc/wireless file, which provides
*           information regarding the connection status quality, link level, 
*           and noise level, and then stores that information for publishing */

void checkWiFiStrength()
{

    if (wifiConnected)
    {
        network_status = getCmdOutput(networkcmdarr);
        network_data = split(network_status, '.');

        network_msg.x = atof(network_data[0].c_str());
        network_msg.y = atof(network_data[1].c_str());
        network_msg.z = atof(network_data[2].c_str());
    }


    else //If not connected to the network output zeros
    {
        network_msg.x = 0.0;
        network_msg.y = 0.0;
        network_msg.z = 0.0;

    }
    


}


/* ********************************************************************************
 *  main 
 **********************************************************************************/ 

int main(int argc, char ** argv)
{


    // Initialize ROS node with name " "
    ros::init(argc, argv, "ugv_watchdog");
    // Create handle to acess node functionality
    ros::NodeHandle nh;
    
    ros::Rate loop_rate(20); // set rate for update loop [Hz]

    //create subscriber on node to get the bluetooth joystick input
    ros::Subscriber bluetooth_sub = nh.subscribe("/bluetooth_teleop/joy", 100, BluetoothSubCallback);
    
    //create subscriber on node to get the wifi connection status
    ros::Subscriber wifi_sub = nh.subscribe("/ugv/wifi_connected", 100, WiFiSubCallback);
        
    //create subscriber on node to get the battery status
    ros::Subscriber battery_sub = nh.subscribe("/status", 100, BatterySubCallback);
    

    //create subscriber on node to get the temperature status
    ros::Subscriber temp_sub = nh.subscribe("/feedback", 100, TempSubCallback);
    
    //create publisher on node to overwrite the old commands and stop the Jackal
    ros::Publisher cmdvel_pub = nh.advertise<geometry_msgs::Twist>("/cmd_vel",100);

    //create publisher on node to get the bluetooth joystick input
    ros::Publisher network_pub = nh.advertise<geometry_msgs::Vector3>("/ugv_network_link_level_noise",100);

    //Publish the state of the emergency flag
    ros::Publisher watchdog_pub = nh.advertise<std_msgs::Bool>("/ugv_emergency", 100);

    

    // ros::ok()==0 occurs when node gets SIGTERM interrupt or similar
    while(ros::ok())
    { 
        //Check timeout
        //check_cmd_timeout();
        check_battV();
        check_temperature();
        checkWiFi();
        checkWiFiStrength();
        
        // block once
        ros::spinOnce();
        // sleep for time defined by loop rate
        loop_rate.sleep();
        //publish netowrk information topics
        network_pub.publish(network_msg);
        //publish emergency flag status
        timeoutmsg.data = timeoutBool;
        watchdog_pub.publish(timeoutmsg);



        //If the Emergency flag is set, publish out new values for 
        // cmd_vel to stop the Jackal from moving
        if (timeoutBool)
        {
            // Initialize cmdvel message
            geometry_msgs::Twist msg;
            // Set all values to zero to stop Jackal
            msg.linear.x  = 0.0;
            msg.linear.y  = 0.0;
            msg.linear.z  = 0.0;
            msg.angular.x = 0.0;
            msg.angular.y = 0.0;
            msg.angular.z = 0.0;
            // Publish new cmdvel message
            cmdvel_pub.publish(msg); 
            ROS_INFO("EMERGENCY_STOP COMPLETE");
        }

    }
    return 0;
    
}
