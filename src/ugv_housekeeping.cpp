/*
*
*
*    Node: UGV Housekeeping 
*    Date: 02/13/2018
*    Author: Rishab Gangopadhyay
*
*    Vehicle: UGV
*
*
*    Purpose: This node throttles topics for compatibility with the UI.
*             It does so by subscribing to topics that are already being 
*             published through the Jackal prebuilt software, and then publishing 
*             out new formatted msgs at a fixed rate of approximately 5 Hz.
*             This low rate allowsfor the UI to read from these topics successfully. 
*        
*
*    Inputs:  WiFi Connection Status (/wifi_connected)
*             Jackal Battery Voltage (/status)
*             Jackal MCU/PCB/Motor Temperatures (/feedback)
*             Network Diagnostics (/ugv_network_link_level_noise_ui)
*             Commanded Velocity (/cmd_vel)
*
*   Outputs:  WiFi Connection Status (/wifi_connected_ui)
*             Jackal Battery Voltage (/ugv_voltage_ui)
*             Jackal MCU/PCB/Motor Temperatures (see code)
*             Network Diagnostics (/ugv_network_link_level_noise_ui)
*             Commanded Velocity (/cmd_vel_ui)
*             Storage Information (/ugv_storage_info)
*
*
*/
/* ********************************************************************************
 *  Packages to Include 
 *
 **********************************************************************************/ 

#include <ros/ros.h>
#include <math.h>
#include <tf/transform_listener.h>
#include "sensor_msgs/Joy.h"
#include "geometry_msgs/Twist.h"
#include "jackal_msgs/Status.h"
#include "jackal_msgs/Feedback.h"

#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float64.h"


/* ********************************************************************************
 *  Variable Definitions
 *
 **********************************************************************************/ 

// Timeout Emergency Flag; Is set when thresholds met
bool timeoutBool = 0;                                
// Initialize msgs to be able to read from Diagnostics Topic

//Diagnostics Variables Initialization
std_msgs::Float64 battV;
std_msgs::Float64 pcb_temp;
std_msgs::Float64 mcu_temp;
std_msgs::Float64 motor0_temp;
std_msgs::Float64 motor1_temp;


//Variables used to get network status
std::string network_status;
vector<string> network_data;
std_msgs::Bool wifiConnected;
geometry_msgs::Vector3 network_msg;
geometry_msgs::Twist cmdvel_msg;

//Variables for storage information determination
const char storage_info_cmdarr[] = "df -h -BG |grep /dev/sda1 | awk '{print $2$3$4}' 2>&1";
std::string storage_info;
vector<string> storage_data;
geometry_msgs::Vector3 storage_data_vec;

/* ********************************************************************************
 *  Callback Functions 
 *
 *  These callback functions are called by the ROS subscribers defined in main 
 *  whenever a new topic value comes in. The primary role of these callback 
 *  functions is to store a local copy of the most recent telemetry so that they
 *  can be output as required to maintain a 5Hz output
 **********************************************************************************/ 

// TempSubCallback: Used to get temperature from motor feedback topic
void TempSubCallback(const jackal_msgs::Feedback::ConstPtr& msg)
{
    pcb_temp.data = msg->pcb_temperature;
    mcu_temp.data = msg->mcu_temperature;
    motor0_temp.data = msg->drivers[0].motor_temperature;   
    motor1_temp.data = msg->drivers[1].motor_temperature;

}

// BatterySubCallback: Used to save off battery status 
void BatterySubCallback(const jackal_msgs::Status::ConstPtr& msg)
{
    battV.data = msg->measured_battery;
}


// WiFiSubCallback
void WiFiSubCallback(const std_msgs::Bool::ConstPtr& msg)
{

    wifiConnected.data = msg->data;

}


void NetworkSubCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{   
    network_msg.x = msg->x;
    network_msg.y = msg->y;
    network_msg.z = msg->z;

}

void CmdVelCallback(const geometry_msgs::Twist::ConstPtr& msg)
{   
    cmdvel_msg.linear = msg->linear;
    cmdvel_msg.angular = msg->angular;

}


/* ********************************************************************************
 *  Helper Functions 
 *
 **********************************************************************************/ 

/* getCmdOutput
*
*  Purpose: to run a command line argument and store the output locally 
*  Modified from code by Vittorio Romeo 
*  https://codereview.stackexchange.com/questions/42148/
*  running-a-shell-command-and-getting-output */
std::string getCmdOutput(const std::string& mStr)
{
    std::string result, file;
    FILE* pipe{popen(mStr.c_str(), "r")};
    char buffer[256];

    while(fgets(buffer, sizeof(buffer), pipe) != NULL)
    {
        file = buffer;
        result += file.substr(0, file.size() - 1);
    }

    pclose(pipe);
    return result;
}

/* split
*
* Purpose: to take a string variable and split it into its various elements
*          by some predetermined delimiter
 http://ysonggit.github.io/coding/2014/12/16/split-a-string-using-c.html */
vector<string> split(const string &s, char delim) {
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

/* getStorageInfo
*
*  Purpose: this fumction spawns a shell command to run a command line
*           function in order to determine the storage space stize, 
*           used and avalailable on the mounted mmcblk1p1 drive. 
*           It then stores this information in a new message for 
*           publishing out.                                            */

void getStorageInfo()
{

        storage_info = getCmdOutput(storage_info_cmdarr);
        if(!storage_info.empty())
    {
       storage_data = split(storage_info, 'G');
           storage_data_vec.x = atof(storage_data[0].c_str()); //Size
           storage_data_vec.y = atof(storage_data[1].c_str());//used
           storage_data_vec.z = atof(storage_data[2].c_str());//available
    } 
    else //If the correct drive is not yet mounted
    {
       storage_data_vec.x = 0.0;
       storage_data_vec.y = 0.0;
       storage_data_vec.z = 0.0;
    }
}

/* ********************************************************************************
 *  main 
 **********************************************************************************/ 

int main(int argc, char ** argv)
{


    // Initialize ROS node with name " "
    ros::init(argc, argv, "ugv_housekeeping");
    // Create handle to acess node functionality
    ros::NodeHandle nh;
    
    ros::Rate loop_rate(5); // set rate for update loop [Hz]

    
    //create subscriber on node to get the wifi connection status
    ros::Subscriber wifi_sub = nh.subscribe("/wifi_connected", 100, WiFiSubCallback);
    ros::Publisher  wifi_pub = nh.advertise<std_msgs::Bool>("/wifi_connected_ui",100);
    
    //create subscriber on node to get the battery status
    ros::Subscriber battery_sub = nh.subscribe("/status", 100, BatterySubCallback);
    ros::Publisher  battery_pub = nh.advertise<std_msgs::Float64>("/ugv_voltage_ui",100);


    //create subscriber on node to get the temperature status
    ros::Subscriber temp_sub = nh.subscribe("/feedback", 100, TempSubCallback);
    ros::Publisher  pcb_temp_pub = nh.advertise<std_msgs::Float64>("/pcb_temp_ui",100);
    ros::Publisher  mcu_temp_pub = nh.advertise<std_msgs::Float64>("/mcu_temp_ui",100);
    ros::Publisher  motor0_temp_pub = nh.advertise<std_msgs::Float64>("/motor0_temp_ui",100);
    ros::Publisher  motor1_temp_pub = nh.advertise<std_msgs::Float64>("/motor1_temp_ui",100);

    //create subscriber and publisher on node to throttle Wifi information
    ros::Subscriber network_sub = nh.subscribe("/ugv_network_link_level_noise_ui", 100, NetworkSubCallback);
    ros::Publisher  network_pub = nh.advertise<geometry_msgs::Vector3>("/ugv_network_link_level_noise_ui",100);
    //Create subscriber and publisher to throttle commanded velocity
    ros::Subscriber cmdvel_sub = nh.subscribe("/cmd_vel", 100, CmdVelCallback); 
    ros::Publisher cmdvel_pub  = nh.advertise<geometry_msgs::Twist>("/cmd_vel_ui",100);
    //Create publisher to publush storage information
    ros::Publisher storage_info_pub = nh.advertise<geometry_msgs::Vector3>("/ugv_storage_info",100);

    // ros::ok()==0 occurs when node gets SIGTERM interrupt or similar
    while(ros::ok())
    { 
        // block once
        ros::spinOnce();
        // sleep for time defined by loop rate
        loop_rate.sleep();
        //Get storage information
        getStorageInfo();
        //publish storage information topic
        storage_info_pub.publish(storage_data_vec);
        //publish throttled wifi connection status
        wifi_pub.publish(wifiConnected);
        //publish throttled battery voltage topic
        battery_pub.publish(battV);
        //publish throttled PCB temperature topic
        pcb_temp_pub.publish(pcb_temp);
        //publish throttled MCU temperature topic
        mcu_temp_pub.publish(mcu_temp);
        //publish throttled motor temperature topics
        motor0_temp_pub.publish(motor0_temp);
        motor1_temp_pub.publish(motor1_temp);
        //publish wifi connection diagnostics topic
        network_pub.publish(network_msg);
        //publish throttled commaned velocity topic
        cmdvel_pub.publish(cmdvel_msg);
        //Output ROS_INFO to record that we are running the housekeeping node. 
        ROS_INFO("UGV Housekeeping Node Running");

    }
    return 0;
    
}